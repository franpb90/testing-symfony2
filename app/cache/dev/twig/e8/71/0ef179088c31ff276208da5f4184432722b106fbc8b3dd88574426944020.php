<?php

/* fpb90PresentationBundle:Default:index.html.twig */
class __TwigTemplate_e8710ef179088c31ff276208da5f4184432722b106fbc8b3dd88574426944020 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "Hello ";
        if (array_key_exists("name", $context)) {
            // line 2
            echo " ";
            echo twig_escape_filter($this->env, (isset($context["name"]) ? $context["name"] : $this->getContext($context, "name")), "html", null, true);
            echo "
";
        }
        // line 3
        echo "!
";
    }

    public function getTemplateName()
    {
        return "fpb90PresentationBundle:Default:index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  28 => 3,  22 => 2,  19 => 1,);
    }
}
