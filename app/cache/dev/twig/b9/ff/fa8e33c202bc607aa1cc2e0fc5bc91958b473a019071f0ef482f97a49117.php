<?php

/* PresentationBundle:Default:index.html.twig */
class __TwigTemplate_b9fffa8e33c202bc607aa1cc2e0fc5bc91958b473a019071f0ef482f97a49117 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("PresentationBundle::layout.html.twig");

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'content_header' => array($this, 'block_content_header'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "PresentationBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        echo "Ultimate Compressor";
    }

    // line 5
    public function block_content_header($context, array $blocks = array())
    {
        echo "";
    }

    // line 7
    public function block_content($context, array $blocks = array())
    {
        // line 8
        echo "    ";
        $context["version"] = ((twig_constant("Symfony\\Component\\HttpKernel\\Kernel::MAJOR_VERSION") . ".") . twig_constant("Symfony\\Component\\HttpKernel\\Kernel::MINOR_VERSION"));
        // line 9
        echo "
    <h1 class=\"title\">Welcome!</h1>

\t<div style=\"width:350px;\"class=\"form\">
    <p>Upload the file what you want compress</p>
\t<form action=\"demo_form.asp\" method=\"post\">
\t<input style=\"display:block;\" name=\"uncompressfile\" type=\"file\"></input>
\t<input style=\"float:right;margin-top:8px;\" type=\"submit\" value=\"Submit\">
\t</form>
\t</div>
";
    }

    public function getTemplateName()
    {
        return "PresentationBundle:Default:index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  48 => 9,  45 => 8,  42 => 7,  36 => 5,  30 => 3,);
    }
}
