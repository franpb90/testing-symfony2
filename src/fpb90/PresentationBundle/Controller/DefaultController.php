<?php

namespace fpb90\PresentationBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('fpb90PresentationBundle:Default:index.html.twig');
    }
}
