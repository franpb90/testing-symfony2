<?php

namespace fpb90\UltimateCompressorBundle\Controller;

use fpb90\UltimateCompressorBundle\Entity\File;
use fpb90\UltimateCompressorBundle\Form\Type\FileType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Translation\Translator;
use Symfony\Component\Translation\MessageSelector;
use Symfony\Component\Translation\Loader\XliffFileLoader;

class DefaultController extends Controller
{
    public function indexAction()
    {
        $file = new File();
        $form = $this->createForm(new FileType(), $file, array(
            'action' => $this->generateUrl('fpb90_ultimate_compressor_uploadaction')
        ));

        return $this->render('fpb90UltimateCompressorBundle:Default:index.html.twig', array(
            'form' => $form->createView(),
        ));
    }
    
    public function newAction()
    {

    }
    
    public function uploadAction(Request $request)
    {
        $file = new File();
        $form = $this->createForm(new FileType(), $file, array(
            'action' => $this->generateUrl('fpb90_ultimate_compressor_uploadaction')
        ));
        
        $form->handleRequest($request);

        if ($form->isValid()){
            
            $em = $this->getDoctrine()->getManager();
            
            
            $file->upload();
            $em->persist($file);
            $em->flush();
            
            return $this->render('fpb90UltimateCompressorBundle:Default:index.html.twig', array(
              'message' => $file->getName() . ' has been uploaded successfully'
            ));
        }

       // $request->setLocale('es');
         $translator = new Translator('es_SP', new MessageSelector());
         $translator->addLoader('xliff', new XliffFileLoader());
         $translator->addResource('xliff', 'messages.es.xliff', 'en');
         
         
         return $this->render('fpb90UltimateCompressorBundle:Default:index.html.twig', array(
           'message' => $translator->trans('file.maxsize'),
           'form'    => $form->createView()
         ));
    }
}
