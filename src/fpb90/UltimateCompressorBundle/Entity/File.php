<?php

namespace fpb90\UltimateCompressorBundle\Entity;


use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * File
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class File
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * 
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="size", type="bigint", nullable=false)
     */
    private $size;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=100, nullable=true)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="path", type="string", length=100, nullable=true)
     */
    private $path;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateUploaded", type="datetime", nullable=true)
     */
    private $dateUploaded;

    /**
     * @ORM\ManyToOne(targetEntity="fpb90\PresentationBundle\Entity\User", inversedBy="file")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", nullable=true)
     */
    private $owner;
    
    
    /**
     * @Assert\File(maxSize="160k", maxSizeMessage = "file.maxsize")
     */
    private $file;
    
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set size
     *
     * @param integer $size
     * @return File
     */
    public function setSize($size)
    {
        $this->size = $size;

        return $this;
    }

    /**
     * Get size
     *
     * @return integer 
     */
    public function getSize()
    {
        return $this->size;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return File
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set path
     *
     * @param string $path
     * @return File
     */
    public function setPath($path)
    {
        $this->path = $path;

        return $this;
    }

    /**
     * Get path
     *
     * @return string 
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * Set dateUploaded
     *
     * @param \DateTime $dateUploaded
     * @return File
     */
    public function setDateUploaded($dateUploaded)
    {
        $this->dateUploaded = $dateUploaded;

        return $this;
    }

    /**
     * Get dateUploaded
     *
     * @return \DateTime 
     */
    public function getDateUploaded()
    {
        return $this->dateUploaded;
    }

    /**
     * Set owner
     *
     * @param \franpb90\Presentation\Entity\User $owner
     * @return File
     */
    public function setOwner(\franpb90\Presentation\Entity\User $owner = null)
    {
        $this->owner = $owner;

        return $this;
    }

    /**
     * Get owner
     *
     * @return \franpb90\Presentation\Entity\User 
     */
    public function getOwner()
    {
        return $this->owner;
    }
    
    /**
     * Sets file.
     *
     * @param UploadedFile $file
     */
    public function setFile(UploadedFile $file = null)
    {
        $this->file = $file;
    }

    /**
     * Get file.
     *
     * @return UploadedFile
     */
    public function getFile()
    {
        return $this->file;
    }
    
    protected function getUploadRootDir()
    {
        // the absolute directory path where uploaded
        // documents should be saved
        return __DIR__.'/../../../../web/'.$this->getUploadDir();
    }

    protected function getUploadDir()
    {
        // get rid of the __DIR__ so it doesn't screw up
        // when displaying uploaded doc/image in the view.
        return 'uploads/files';
    }
    
    public function getAbsolutePath()
    {
        return null === $this->path
            ? null
            : $this->getUploadRootDir().'/'.$this->path;
    }

    public function getWebPath()
    {
        return null === $this->path
            ? null
            : $this->getUploadDir().'/'.$this->path;
    }

    public function upload()
    {
        // the file property can be empty if the field is not required
        $uploadedFile = $this->file;
        if (null === $uploadedFile) {
            //$this->size = 0;
            return;
        }

        // move takes the target directory and then the
        // target filename to move to
        $uploadedFile->move(
            $this->getUploadRootDir(),
            $uploadedFile->getClientOriginalName()
        );

       // set the path property to the filename where you've saved the file
       $this->path = $uploadedFile->getClientOriginalName();
        
       $this->name = $uploadedFile->getClientOriginalName();
        
       $this->size = $uploadedFile->getClientSize();
        
       $this->dateUploaded = new \DateTime(date('Y-m-d H:i:s'));
    }
}
